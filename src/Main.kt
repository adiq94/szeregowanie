import java.io.File
import java.lang.Math.floor
import kotlin.math.roundToInt
import kotlin.random.Random

fun main(args: Array<String>) {
    val sch10 = getResourceAsText("/sch10.txt")
    val sch50 = getResourceAsText("/sch50.txt")
    val sch100 = getResourceAsText("/sch100.txt")
    val sch1000 = getResourceAsText("/sch1000.txt")

    val sch10problems = parseProblems(sch10)
    val sch50problems = parseProblems(sch50)
    val sch100problems = parseProblems(sch100)
    val sch1000problems = parseProblems(sch1000)

    val builder = StringBuilder()
    val csvBuilder = StringBuilder()
    builder.appendln("Genetic:")
    runGenetic(sch10problems, 0.8, builder, csvBuilder)
    runGenetic(sch50problems, 0.6, builder, csvBuilder)
    runGenetic(sch100problems, 0.4, builder, csvBuilder)
    runGenetic(sch1000problems, 0.2, builder, csvBuilder)
    builder.appendln("Genetic max time:")
    runGeneticTime(sch10problems, 0.8, builder, csvBuilder, 60 * 1000)
    runGeneticTime(sch50problems, 0.6, builder, csvBuilder, 60 * 1000)
    runGeneticTime(sch100problems, 0.4, builder, csvBuilder, 60 * 1000)
    runGeneticTime(sch1000problems, 0.2, builder, csvBuilder,  60 * 1000)
    File("result.txt").writeText(builder.toString())
    File("result.csv").writeText(csvBuilder.toString())

}

private fun bestResults(n: Int, k: Int, h: Double) : Int{
    when {
        n == 10 && h == 0.8 -> when (k) {
            1 -> return 818
            2 -> return 615
            3 -> return 793
            4 -> return 803
            5 -> return 521
            6 -> return 755
            7 -> return 1083
            8 -> return 540
            9 -> return 554
            10 -> return 671
        }
        n == 50 && h == 0.6 -> when(k) {
            1 -> return 17990
            2 -> return 14231
            3 -> return 16497
            4 -> return 14105
            5 -> return 14650
            6 -> return 14251
            7 -> return 17715
            8 -> return 21367
            9 -> return 14298
            10 -> return 14377

        }
        n == 100 && h == 0.4 -> when(k) {
            1 -> return 89588
            2 -> return 74854
            3 -> return 85363
            4 -> return 87730
            5 -> return 76424
            6 -> return 86724
            7 -> return 79854
            8 -> return 95361
            9 -> return 73605
            10 -> return 72399
        }
        n == 1000 && h == 0.2 -> when(k) {
            1 -> return 15190371
            2 -> return 13356727
            3 -> return 12919259
            4 -> return 12705290
            5 -> return 13276868
            6 -> return 12236080
            7 -> return 14160773
            8 -> return 13314723
            9 -> return 12433821
            10 -> return 13395234
        }
    }
    return 0
}

private fun runGenetic(problems: ArrayList<Problem>, h: Double, builder: StringBuilder, csvBuilder: StringBuilder) : java.lang.StringBuilder {
    val n = problems[0].jobs.count()
    var relativeErrorAll = 0.0
    val fs = ArrayList<Int>()
    builder.appendln("N: $n, H: $h")
    for (i in problems.indices) {
        val p = problems[i]
        val d = p.d(h)
        val startTime = System.currentTimeMillis()
        builder.appendln("K: ${i + 1}")
        val genetic = genetic(p, d)
        val stopTime = System.currentTimeMillis()
        val runTime = stopTime - startTime
        builder.appendln("Time: $runTime ms")
        val f = genetic.split("\\s".toRegex(), 2)[0].toInt()
        val referenceF = bestResults(n, i + 1, h)
        val error = f - referenceF
        fs.add(error)
        val relativeError = error.toDouble() / referenceF.toDouble()
        relativeErrorAll += relativeError
        val relativeErrorPercentage = relativeError * 100
        val formatted = String.format("%.2f", relativeErrorPercentage)
        builder.appendln("Optimal value: $referenceF")
        builder.appendln("Error $formatted%")
        builder.appendln(genetic)
        val r = genetic.split("\\s".toRegex(), 3)[1].toInt()
        csvBuilder.appendln("${i+1};$r;$runTime;$f;$referenceF;$formatted" )
    }
    val meanError = (relativeErrorAll / problems.count().toDouble()) * 100
    val formatted = String.format("%.4f", meanError)
    val sd = calculateSD(fs.map{ it.toDouble()}.toDoubleArray())
    builder.appendln("Mean Error $formatted% , Standard Deviation: $sd")
    csvBuilder.appendln()
    csvBuilder.appendln(formatted)
    csvBuilder.appendln(sd)
    csvBuilder.appendln()
    return builder
}

fun calculateSD(numArray: DoubleArray): Double {
    var sum = 0.0
    var standardDeviation = 0.0

    for (num in numArray) {
        sum += num
    }

    val mean = sum / 10

    for (num in numArray) {
        standardDeviation += Math.pow(num - mean, 2.0)
    }

    return Math.sqrt(standardDeviation / 10)
}

private fun runGeneticTime(problems: ArrayList<Problem>, h: Double, builder: StringBuilder, csvBuilder: StringBuilder, time: Int) : java.lang.StringBuilder {
    val n = problems[0].jobs.count()
    var relativeErrorAll = 0.0
    val fs = ArrayList<Int>()
    builder.appendln("N: $n, H: $h")
    for (i in problems.indices) {
        val p = problems[i]
        val d = p.d(h)
        val startTime = System.currentTimeMillis()
        builder.appendln("K: ${i + 1}")
        val genetic = geneticTime(p, d, time)
        val stopTime = System.currentTimeMillis()
        val runTime = stopTime - startTime
        builder.appendln("Time: $runTime ms")
        val f = genetic.split("\\s".toRegex(), 2)[0].toInt()
        val referenceF = bestResults(n, i + 1, h)
        val error = f - referenceF
        fs.add(error)
        val relativeError = error.toDouble() / referenceF.toDouble()
        relativeErrorAll += relativeError
        val relativeErrorPercentage = relativeError * 100
        val formatted = String.format("%.2f", relativeErrorPercentage)
        builder.appendln("Optimal value: $referenceF")
        builder.appendln("Error $formatted%")
        builder.appendln(genetic)
        val r = genetic.split("\\s".toRegex(), 3)[1].toInt()
        csvBuilder.appendln("${i+1};$r;$runTime;$f;$referenceF;$formatted" )
    }
    val meanError = (relativeErrorAll / problems.count().toDouble()) * 100
    val formatted = String.format("%.4f", meanError)
    val sd = calculateSD(fs.map{ it.toDouble()}.toDoubleArray())
    builder.appendln("Mean Error $formatted% , Standard Deviation: $sd")
    csvBuilder.appendln()
    csvBuilder.appendln(formatted)
    csvBuilder.appendln(sd)
    csvBuilder.appendln()
    return builder
}

private fun geneticTime(problem: Problem, d: Int, t: Int): String {
    val startTime = System.currentTimeMillis()
    val initialPopulation = ArrayList<Problem>()
    problem.setInitialGenes()
    initialPopulation.add(problem)
    for(i in 0..19){
        val randomProblem = Problem(problem.jobs)
        randomProblem.setRandomGenes()
        initialPopulation.add(randomProblem)
    }
    var bestF = 100000000
    var secondBestF = 100000000
    var worstF = 0
    var best : Problem? = null
    var secondBest : Problem? = null
    var worstIndex = 0
    var bestOrder = ""
    var generation = 0
    var generationWithoutImprovement = 0
    while(System.currentTimeMillis() - startTime < t && generationWithoutImprovement < 100000) {
        for (i in 0..20) {
            val left = initialPopulation[i].sortedLeft()
            val right = initialPopulation[i].sortedRight()
            val leftSum = initialPopulation[i].leftSum()
            val r = Math.max(0, d - leftSum)
            val splited = left + right
            val solution = ProblemSolution(r, d, splited)
            val f = solution.f()
            when {
                f < bestF -> {
                    generationWithoutImprovement = 0
                    bestF = f
                    best = initialPopulation[i]
                    bestOrder = solution.toString()
                }
                f < secondBestF -> {
                    secondBestF = f
                    secondBest = initialPopulation[i]
                }
                f > worstF -> {
                    worstF = f
                    worstIndex = i
                }
            }
        }
        var cross = best!!.uniformCrossover(secondBest!!)
        if(Random.nextInt(0,100) > 70){
            cross = cross.mutate()
        }
        initialPopulation[worstIndex] = cross
        generation++
        generationWithoutImprovement++
        worstF = 0
    }
    System.out.println("Generations: $generation")
    return bestOrder
}

private fun genetic(problem: Problem, d: Int): String {
    val initialPopulation = ArrayList<Problem>()
    problem.setInitialGenes()
    initialPopulation.add(problem)
    for(i in 0..19){
        val randomProblem = Problem(problem.jobs)
        randomProblem.setRandomGenes()
        initialPopulation.add(randomProblem)
    }
    var bestF = 100000000
    var secondBestF = 100000000
    var worstF = 0
    var best : Problem? = null
    var secondBest : Problem? = null
    var worstIndex = 0
    var bestOrder = ""
    var generation = 0
    var generationWithoutImprovement = 0
    while(generationWithoutImprovement < 1000) {
        for (i in 0..20) {
            val left = initialPopulation[i].sortedLeft()
            val right = initialPopulation[i].sortedRight()
            val leftSum = initialPopulation[i].leftSum()
            val r = Math.max(0, d - leftSum)
            val splited = left + right
            val solution = ProblemSolution(r, d, splited)
            val f = solution.f()
            when {
                f < bestF -> {
                    generationWithoutImprovement = 0
                    bestF = f
                    best = initialPopulation[i]
                    bestOrder = solution.toString()
                }
                f < secondBestF -> {
                    secondBestF = f
                    secondBest = initialPopulation[i]
                }
                f > worstF -> {
                    worstF = f
                    worstIndex = i
                }
            }
        }
        var cross = best!!.uniformCrossover(secondBest!!)
        if(Random.nextInt(0,100) > 70){
            cross = cross.mutate()
        }
        initialPopulation[worstIndex] = cross
        generation++
        generationWithoutImprovement++
        worstF = 0
    }
    System.out.println("Generations: $generation")
    return bestOrder
}

fun parseProblems(content: String): ArrayList<Problem> {

    val lines = content.lines().drop(1)
    val problems = ArrayList<Problem>()
    var currentLine = 0
    while (currentLine < lines.size - 1) {
        val numOfLines = lines[currentLine].trim().toInt()
        val jobs = ArrayList<Job>()
        currentLine++
        for (i in currentLine until currentLine + numOfLines) {
            val items = lines[i].split("\\s+".toRegex()).drop(1)
            jobs.add(Job(items[0].toInt(), items[1].toInt(), items[2].toInt()))
            currentLine++
        }
        problems.add(Problem(jobs))
    }
    return problems
}

fun getResourceAsText(path: String): String {
    return object {}.javaClass.getResource(path).readText()
}

data class Job(val p: Int, val a: Int, val b: Int)

class Problem(val jobs: ArrayList<Job>) {
    var genes : ArrayList<Int> = ArrayList()

    fun setRandomGenes(){
        genes.clear()
        clearCache()
        for(job in jobs){
            genes.add(Random.nextInt(0,2))
        }
    }

    fun setInitialGenes() {
        genes.clear()
        clearCache()
        for(job in jobs){
            var value = 0
            if(job.a > job.b) value = 1
            genes.add(value)
        }
    }

    fun uniformCrossover(p : Problem) : Problem{
        val result = ArrayList<Int>()
        for(i in 0 until genes.size){
            if(Random.nextInt(0,100) > 50){
                result.add(genes[i])
            } else{
                result.add(p.genes[i])
            }
        }
        val newProblem = Problem(jobs)
        newProblem.genes = result
        return newProblem
    }

    fun mutate() : Problem{
        val point = Random.nextInt(1, genes.size - 1)
        val point2 = Random.nextInt(1, genes.size - 1)
        val result = ArrayList<Int>(genes.size)
        for(i in 0 until genes.size){
            result.add(genes[i])
        }
        if(result[point] == 0){
            result[point] = 1
        } else{
            result[point] = 0
        }
        if(point != point2){
            if(result[point2] == 0){
                result[point2] = 1
            } else{
                result[point2] = 0
            }
        }
        val newProblem = Problem(jobs)
        newProblem.genes = result
        return newProblem
    }
    var p : Int? = null
    fun sumP(): Int {
        if(p == null){
            p = jobs.sumBy { it.p }
        }
        return p!!
    }

    fun d(h: Double): Int {
        return floor(sumP() * h).roundToInt()
    }

    fun left() = sequence {
        for(i in 0 until genes.count()){
            if(genes[i] == 0) yield(jobs[i])
        }
    }
    fun right() = sequence {
        for(i in 0 until genes.count()){
            if(genes[i] == 1) yield(jobs[i])
        }
    }
    var leftCache: List<Job>? = null
    var rightCache: List<Job>? = null
    fun clearCache(){
        leftCache = null
        rightCache = null
    }
    fun sortedLeft() : List<Job>{
        if(leftCache == null) {
            leftCache = left().sortedBy { it.a.toDouble() / it.p.toDouble() }.toList()
        }
        return leftCache!!
    }
    fun sortedRight() : List<Job>{
        if(rightCache == null) {
            rightCache = right().sortedByDescending { it.b.toDouble() / it.p.toDouble() }.toList()
        }
        return rightCache!!
    }
    var sum : Int? = null
    fun leftSum(): Int{
        if(sum == null){
            sum = sortedLeft().sumBy { it.p }
        }
        return sum!!
    }
}

class ProblemSolution(val r: Int, val d: Int, val jobs: List<Job>) {
    fun f(): Int {
        var F = 0
        var time = r
        for (job in jobs) {
            time += job.p
            F += if (time <= d) {
                (d - time) * job.a
            } else {
                (time - d) * job.b
            }
        }
        return F
    }

    override fun toString(): String {
        val builder = StringBuilder()
        val F = f()
        builder.append("$F $r $d")
        for (job in jobs) {
            builder.append(" ${job.p} ${job.a} ${job.b}")
        }
        return builder.toString()
    }
}